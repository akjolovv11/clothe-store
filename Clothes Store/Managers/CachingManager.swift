//
//  CachingManager.swift
//  Clothes Store
//
//  Created by jones on 7/11/22.
//

import UIKit
import RealmSwift

class CachingManager: NSObject {

    static let instance = CachingManager()
    private let realm = try! Realm()
    
    func writeProduct(product: Product){
        do {
            try realm.write {
                realm.add(product)
            }
        } catch (let error){
            print("Error writeCoin: \(error.localizedDescription)")
        }
    }
    
    func updateProduct(uuid: String,
                       name: String,
                       price: Int,
                       color: String,
                       size: String,
                       articul: String,
                       count: Int,
                       manufacturer: String,
                       brand: String) {
        
        let realm = try! Realm()
        
        guard let theProduct = realm.object(ofType: Product.self, forPrimaryKey: uuid) else { return }
        
        try! realm.write {
            theProduct.name = name
            theProduct.price = price
            theProduct.color = color
            theProduct.size = size
            theProduct.articul = articul
            theProduct.count = count
            theProduct.manufacturer = manufacturer
            theProduct.brand = brand
        }
    }
    
    func getAllProducts() -> Results<Product> {
        return realm.objects(Product.self)
    }
    
    func getProducts(uuid: String) -> Product? {
        return realm.object(ofType: Product.self, forPrimaryKey: uuid)
    }
}
