//
//  UIStoryboard+.swift
//  Clothes Store
//
//  Created by jones on 7/11/22.
//

import UIKit

extension UIStoryboard {
    class func createVC<T: UIViewController>(storyboardName: String, controllerType: T.Type) -> T {
        let st = UIStoryboard.init(name: storyboardName, bundle: nil)
        let vc = st.instantiateViewController(withIdentifier: controllerType.className) as! T
        return vc
    }
}
