//
//  NSObject+.swift
//  Clothes Store
//
//  Created by jones on 7/11/22.
//

import UIKit

extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
    
}
