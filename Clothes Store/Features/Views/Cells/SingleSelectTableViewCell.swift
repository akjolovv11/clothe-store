//
//  SingleSelectTableViewCell.swift
//  Shoppix
//
//  Created by Sagyndyk Akzholov on 4/5/21.
//  Copyright © 2021 PeakSoft. All rights reserved.
//

import UIKit

class SingleSelectTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
