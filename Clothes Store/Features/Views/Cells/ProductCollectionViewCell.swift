//
//  ProductCollectionViewCell.swift
//  Clothes Store
//
//  Created by jones on 6/11/22.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var articulLabel: UILabel!
    @IBOutlet weak var manufacturerLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupWith(product: Product) {
        nameLabel.text = product.name
        priceLabel.text = "Цена: \(product.price) сом"
        colorLabel.text = "Цвет: " + product.color
        sizeLabel.text = "Размер: " + product.size
        articulLabel.text = "Артикул: " + product.articul
        countLabel.text = "Количество: \(product.count)"
        manufacturerLabel.text = "Производитель: \(product.manufacturer)"
        brandLabel.text = "Бренд: \(product.brand)"
    }
}
