//
//  Brand.swift
//  Clothes Store
//
//  Created by jones on 7/11/22.
//

import Foundation

struct Brand {
    let id: Int
    let name: String
}
