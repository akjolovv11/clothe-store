//
//  Manufacturer.swift
//  Clothes Store
//
//  Created by jones on 7/11/22.
//

import Foundation

struct Manufacturer {   
    let id: Int
    let name: String
    let brands: [Brand]
}
