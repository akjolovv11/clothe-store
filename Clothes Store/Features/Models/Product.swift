//
//  Product.swift
//  Clothes Store
//
//  Created by jones on 7/11/22.
//

import Foundation
import RealmSwift

class Product: Object {
    
    @objc dynamic var uuid: String = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var price: Int = 0
    @objc dynamic var color: String = ""
    @objc dynamic var size: String = ""
    @objc dynamic var articul: String = ""
    @objc dynamic var count: Int = 0
    @objc dynamic var manufacturer: String = ""
    @objc dynamic var brand: String = ""
    
    override static func primaryKey() -> String? {
        return "uuid"
    }
    
    convenience init(name: String, price: Int, color: String, size: String, articul: String, count: Int, manufacturer: String, brand: String) {
        self.init()
        self.name = name
        self.price = price
        self.color = color
        self.size = size
        self.articul = articul
        self.count = count
        self.manufacturer = manufacturer
        self.brand = brand
    }
}
