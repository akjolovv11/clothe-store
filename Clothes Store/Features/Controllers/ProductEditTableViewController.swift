//
//  ProductEditTableViewController.swift
//  Clothes Store
//
//  Created by jones on 7/11/22.
//

import UIKit

protocol ProductEditTableViewControllerDelegate: AnyObject {
    func updateProducts(uuid: String)
}

class ProductEditTableViewController: UITableViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var colorTextField: UITextField!
    @IBOutlet weak var sizeTextField: UITextField!
    @IBOutlet weak var articulTextField: UITextField!
    @IBOutlet weak var countTextField: UITextField!
    @IBOutlet weak var manufacturerLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    
    weak var delegate: ProductEditTableViewControllerDelegate?
    
    private var selectedManufacturer: Manufacturer? {
        didSet {
            manufacturerLabel.text = selectedManufacturer?.name
            tableView.reloadData()
        }
    }
    private var selectedBrand: Brand? {
        didSet {
            brandLabel.text = selectedBrand?.name
        }
    }
    var product: Product?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        nameTextField.text = product?.name
        priceTextField.text = "\(product?.price ?? 0)"
        colorTextField.text = product?.color
        sizeTextField.text = product?.size
        articulTextField.text = product?.articul
        countTextField.text = "\(product?.count ?? 0)"
        manufacturerLabel.text = product?.manufacturer
        brandLabel.text = product?.brand
    }
    
    @IBAction func didPressUpdateBarButton(_ sender: UIBarButtonItem) {
        guard let name = nameTextField.text else {
            showAlert(title: "Ошибка!", messageBody: "Введите название")
            return
        }
        
        guard let price = Int(priceTextField.text ?? "") else {
            showAlert(title: "Ошибка!", messageBody: "Введите цену")
            return
        }
        
        guard let size = sizeTextField.text else {
            showAlert(title: "Ошибка!", messageBody: "Введите размер")
            return
        }
        
        guard let color = colorTextField.text else {
            showAlert(title: "Ошибка!", messageBody: "Введите цвет")
            return
        }
        
        guard let articul = articulTextField.text else {
            showAlert(title: "Ошибка!", messageBody: "Введите артикул")
            return
        }
        
        guard let count = Int(countTextField.text ?? "") else {
            showAlert(title: "Ошибка!", messageBody: "Введите количество")
            return
        }
        
        guard let manufacturer = selectedManufacturer else {
            showAlert(title: "Ошибка!", messageBody: "Выберите производителя")
            return
        }
        
        guard let brand = selectedBrand else {
            showAlert(title: "Ошибка!", messageBody: "Выберите бренд")
            return
        }
        
        guard let product = product else {
            showAlert(title: "Ошибка!", messageBody: "Что-то случилось не так, повторите позже")
            return
        }
        
        CachingManager.instance.updateProduct(uuid: product.uuid,
                                              name: name,
                                              price: price,
                                              color: color,
                                              size: size,
                                              articul: articul,
                                              count: count,
                                              manufacturer: manufacturer.name,
                                              brand: brand.name)
        delegate?.updateProducts(uuid: product.uuid)
        navigationController?.popViewController(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 6:
            showManufacturerPage()
        case 7:
            showBrandPage()
        default:
            break
        }
    }
    
    private func showManufacturerPage() {
        let vc = UIStoryboard.createVC(storyboardName: "Main", controllerType: ManufacturerSelectTableViewController.self)
        vc.items = getManufacturers()
        vc.dataRangeCallback = { [weak self] selected in
            if selected?.name != self?.selectedManufacturer?.name {
                self?.selectedBrand = nil
                self?.brandLabel.text = "Выберите бренд"
            }
            self?.selectedManufacturer = selected
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func showBrandPage() {
        let vc = UIStoryboard.createVC(storyboardName: "Main", controllerType: BrandSelectTableViewController.self)
        vc.items = selectedManufacturer?.brands ?? []
        vc.dataRangeCallback = { [weak self] selected in
            self?.selectedBrand = selected
        }
        navigationController?.pushViewController(vc, animated: true)
    }
}
