//
//  ViewController.swift
//  Clothes Store
//
//  Created by jones on 6/11/22.
//

import UIKit

class HomeCollectionViewController: UICollectionViewController {

    let products = CachingManager.instance.getAllProducts()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        collectionView.register(ProductCollectionViewCell.nib, forCellWithReuseIdentifier: ProductCollectionViewCell.identifier)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
    }

    @IBAction func didPressFilterBarButton(_ sender: UIBarButtonItem) {
        
    }
    
    @IBAction func didPressSortBarButton(_ sender: UIBarButtonItem) {
        
    }
    
    @IBAction func didPressAddBarButton(_ sender: UIBarButtonItem) {
        let vc = UIStoryboard.createVC(storyboardName: "Main", controllerType: ProductCreateTableViewController.self)
        vc.delegate = self
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.identifier, for: indexPath) as! ProductCollectionViewCell
        cell.setupWith(product: products[indexPath.row])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard.createVC(storyboardName: "Main", controllerType: ProductDetailTableViewController.self)
        vc.product = products[indexPath.row]
        vc.delegate = self
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomeCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2 - 20, height: 260)
    }
}

extension HomeCollectionViewController: ProductCreateTableViewControllerDelegate {
    func reloadProducts() {
        collectionView.reloadData()
    }
}

extension HomeCollectionViewController: ProductDetailTableViewControllerDelegate {
    func needUpdateView() {
        collectionView.reloadData()
    }
}
