//
//  ProductDetailTableViewController.swift
//  Clothes Store
//
//  Created by jones on 7/11/22.
//

import UIKit

protocol ProductDetailTableViewControllerDelegate: AnyObject {
    func needUpdateView()
}

class ProductDetailTableViewController: UITableViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var articulLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var manufacturerLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    
    weak var delegate: ProductDetailTableViewControllerDelegate?
    
    var product: Product?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    @IBAction func didPressEditBarButton(_ sender: UIBarButtonItem) {
        let vc = UIStoryboard.createVC(storyboardName: "Main", controllerType: ProductEditTableViewController.self)
        vc.product = product
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func setupUI() {
        nameLabel.text = product?.name
        if let price = product?.price {
            priceLabel.text = "\(price) сом"
        } else {
            priceLabel.text = "--- сом"
        }
        colorLabel.text = product?.color
        sizeLabel.text = product?.size
        articulLabel.text = product?.articul
        if let count = product?.count {
            countLabel.text = "\(count)"
        } else {
            countLabel.text = "---"
        }
        manufacturerLabel.text = product?.manufacturer
        brandLabel.text = product?.brand
    }
}

extension ProductDetailTableViewController: ProductEditTableViewControllerDelegate {
    func updateProducts(uuid: String) {
        product = CachingManager.instance.getProducts(uuid: uuid)
        setupUI()
        delegate?.needUpdateView()
    }
}
