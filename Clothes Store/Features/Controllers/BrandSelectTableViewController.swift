//
//  BrandSelectTableViewController.swift
//  Clothes Store
//
//  Created by jones on 7/11/22.
//

import UIKit

class BrandSelectTableViewController: UITableViewController {
    
    var items: [Brand] = []
    var dataRangeCallback: ((Brand?) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    fileprivate func setupTableView() {
        tableView = UITableView(frame: .zero, style: .plain)
        tableView.register(SingleSelectTableViewCell.nib, forCellReuseIdentifier: SingleSelectTableViewCell.identifier)
        tableView.tableFooterView = UIView(frame: .zero)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SingleSelectTableViewCell.identifier, for: indexPath) as! SingleSelectTableViewCell
        cell.nameLabel.text = items[indexPath.row].name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dataRangeCallback?(items[indexPath.row])
        navigationController?.popViewController(animated: true)
    }
}
