//
//  ProductCreateTableViewController.swift
//  Clothes Store
//
//  Created by jones on 7/11/22.
//

import UIKit

protocol ProductCreateTableViewControllerDelegate: AnyObject {
    func reloadProducts()
}

class ProductCreateTableViewController: UITableViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var sizeTextField: UITextField!
    @IBOutlet weak var colorTextField: UITextField!
    @IBOutlet weak var articulTextField: UITextField!
    @IBOutlet weak var countTextField: UITextField!
    @IBOutlet weak var manufacturerLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    
    weak var delegate: ProductCreateTableViewControllerDelegate?
    
    var manufacturers: [Manufacturer] = []
    
    private var selectedManufacturer: Manufacturer? {
        didSet {
            manufacturerLabel.text = selectedManufacturer?.name
            tableView.reloadData()
        }
    }
    private var selectedBrand: Brand? {
        didSet {
            brandLabel.text = selectedBrand?.name
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didPressSaveBarButton(_ sender: UIBarButtonItem) {
        guard let name = nameTextField.text else {
            showAlert(title: "Ошибка!", messageBody: "Введите название")
            return
        }
        
        guard let price = Int(priceTextField.text ?? "") else {
            showAlert(title: "Ошибка!", messageBody: "Введите цену")
            return
        }
        
        guard let size = sizeTextField.text else {
            showAlert(title: "Ошибка!", messageBody: "Введите размер")
            return
        }
        
        guard let color = colorTextField.text else {
            showAlert(title: "Ошибка!", messageBody: "Введите цвет")
            return
        }
        
        guard let articul = articulTextField.text else {
            showAlert(title: "Ошибка!", messageBody: "Введите артикул")
            return
        }
        
        guard let count = Int(countTextField.text ?? "") else {
            showAlert(title: "Ошибка!", messageBody: "Введите количество")
            return
        }
        
        guard let manufacturer = selectedManufacturer else {
            showAlert(title: "Ошибка!", messageBody: "Выберите производителя")
            return
        }
        
        guard let brand = selectedBrand else {
            showAlert(title: "Ошибка!", messageBody: "Выберите бренд")
            return
        }
        
        let product = Product(name: name, price: price, color: color, size: size, articul: articul, count: count, manufacturer: manufacturer.name, brand: brand.name)
        
        CachingManager.instance.writeProduct(product: product)
        delegate?.reloadProducts()
        navigationController?.popViewController(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 7:
            guard let _ = selectedManufacturer else { return 0 }
            return 64
        default:
            return 64
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 6:
            showManufacturerPage()
        case 7:
            showBrandPage()
        default:
            break
        }
    }
    
    private func showManufacturerPage() {
        let vc = UIStoryboard.createVC(storyboardName: "Main", controllerType: ManufacturerSelectTableViewController.self)
        vc.items = getManufacturers()
        vc.dataRangeCallback = { [weak self] selected in
            self?.selectedManufacturer = selected
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func showBrandPage() {
        let vc = UIStoryboard.createVC(storyboardName: "Main", controllerType: BrandSelectTableViewController.self)
        vc.items = selectedManufacturer?.brands ?? []
        vc.dataRangeCallback = { [weak self] selected in
            self?.selectedBrand = selected
        }
        navigationController?.pushViewController(vc, animated: true)
    }
}
