//
//  Constants.swift
//  Clothes Store
//
//  Created by jones on 7/11/22.
//

import Foundation

func getManufacturers() -> [Manufacturer] {
    var manufacturers = [Manufacturer]()
    
    let brand11 = Brand(id: 1, name: "Бренд11")
    let brand12 = Brand(id: 2, name: "Бренд12")
    let brand13 = Brand(id: 3, name: "Бренд13")
    
    let brand21 = Brand(id: 4, name: "Бренд21")
    let brand22 = Brand(id: 5, name: "Бренд22")
    let brand23 = Brand(id: 6, name: "Бренд23")
    
    let brand31 = Brand(id: 7, name: "Бренд31")
    let brand32 = Brand(id: 8, name: "Бренд32")
    let brand33 = Brand(id: 9, name: "Бренд33")
    
    manufacturers = [
        Manufacturer(id: 1, name: "Производитель1", brands: [brand11, brand12, brand13]),
        Manufacturer(id: 2, name: "Производитель2", brands: [brand21, brand22, brand23]),
        Manufacturer(id: 3, name: "Производитель3", brands: [brand31, brand32, brand33])
    ]
    return manufacturers
}
